/**
 * @jest-environment jsdom
 */

import { expect, test } from '@jest/globals';
import { createElement } from '../src/scripts/utils/createElement.js';

test('Вызов createElement с неверными параметрами должен выбрасывать ошибку', () => {
  expect(() => createElement()).toThrow();

  expect(() => createElement('Wrong argssss')).toThrow();
});

test('Вызов createElement c параметром tag должен возвращать HTMLElement соответствующий tag', () => {
  const node = createElement({ tag: 'div' });
  const isHtmlDivNode = node instanceof HTMLDivElement;

  expect(isHtmlDivNode).toBe(true);
});

test('Вызов createElement c параметром attributes должен возвращать HTMLElement с заданными атрибутами', () => {
  const testId = 'some_id';
  const testClass = 'some_class';
  const testStyle = 'color: tomato';

  const node = createElement({
    tag: 'span',
    attributes: { id: testId, class: testClass, style: testStyle },
  });

  const actualId = node.getAttribute('id');
  expect(actualId).toBe(testId);

  const actualClass = node.getAttribute('class');
  expect(actualClass).toBe(testClass);

  const actualStyle = node.getAttribute('style');
  expect(actualStyle).toBe(testStyle);
});

test('Вызов createElement c массивом child-строк должен конкатенировать строки в innerHTML', () => {
  const child = ['very', 'long', 'inner', 'text'];
  const root = { tag: 'ul', child: child };

  const rootNode = createElement(root);

  const innerHTML = rootNode.innerHTML;
  expect(innerHTML).toBe(child.join(''));
});

test('Вызов createElement c child-объектом должен возвращать HTMLElement с вложенным потомком', () => {
  const child = { tag: 'li', child: ['very', 'long', 'inner', 'text'] };
  const root = { tag: 'ul', child: child };

  const rootNode = createElement(root);

  const childCount = rootNode.children.length;
  expect(childCount).toBe(1);

  const childNode = rootNode.children.item(0);
  expect(childNode).toBeDefined();
});

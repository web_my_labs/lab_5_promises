/**
 * @jest-environment jsdom
 */

import { test, expect, jest } from '@jest/globals';
import { createElementAsync } from '../src/scripts/utils/createElementAsync.js';

const post = {
  id: '1',
  title: 'Post #1',
  content: 'Content for post#1',
};

test('', async () => {
  const fetchFunction = async () => {
    return new Promise((resolve) => resolve(post));
  };
  const mapFunction = (rawResponse) => {
    return {
      tag: 'div',
      attributes: { id: rawResponse.id },
      child: [
        { tag: 'h1', child: rawResponse.title },
        { tag: 'p', child: rawResponse.content },
      ],
    };
  };
  const element = await createElementAsync(fetchFunction, mapFunction);

  expect(element instanceof HTMLDivElement).toBe(true);
  expect(element.children.length).toBe(2);
});
